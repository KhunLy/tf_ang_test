import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoppingListComponent } from './shopping-list.component';
import { FormsModule } from '@angular/forms';
import { RealService } from 'src/app/_services/real.service';
import { RealMock } from 'src/app/_services/real.mock';

describe('ShoppingListComponent', () => {
  let component: ShoppingListComponent;
  let fixture: ComponentFixture<ShoppingListComponent>;
  let compiled: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoppingListComponent ],
      imports: [FormsModule],
      providers: [ {
        provide: RealService, useClass: RealMock
      } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingListComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contains 2 articles', (done) => {
    setTimeout(() => {
      expect(component.articles.length).toEqual(2);
      done();
    }, 2000);
  });

  it('should have an empty list', () => {
    expect(component.articles.length).toEqual(0);
  })

  it('should add 2 items and articleName is null after add', () => {
    component.articleName = 'Sel';
    component.addArticle();
    expect(component.articleName).toBeNull();
    component.articleName = "Poivre";
    component.addArticle();
    expect(component.articles.length).toEqual(2);
  });

  it('should not have duplicate item', () => {
    component.articleName = 'Sel';
    component.addArticle();
    component.articleName = "Sel";
    component.addArticle();
    expect(component.articles.filter(x => x == 'Sel').length).toEqual(1);
  });

  it('should render an empty list', () => {
    let liList = compiled.querySelectorAll('ul>li');
    expect(liList.length).toEqual(0);
  });
  it('sould render two items', () => {
    component.articleName = 'Sel';
    component.addArticle();
    component.articleName = "Poivre";
    component.addArticle();
    fixture.detectChanges();
    let liList = compiled.querySelectorAll('ul>li');
    expect(liList.length).toEqual(2);
  });

  it('add an item if input != null and button click', () => {
    let input = compiled.querySelector('input');
    input.value = "sel";
    let button = compiled.querySelector('button');
    button.click();
    fixture.detectChanges();
    expect(component.articles.length).toEqual(1);
  });


});
