import { Component, OnInit } from '@angular/core';
import { RealService } from 'src/app/_services/real.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss']
})
export class ShoppingListComponent implements OnInit {
  
  public articles: string[];
  
  public articleName: string;

  constructor(
    private service: RealService
  ) { }

  ngOnInit() {
    this.articles = [];
    this.service.getItems().subscribe(data => {
      this.articles = data
    });
  }

  addArticle() {
    if(this.articles.includes(this.articleName))
      return;
    this.articles.push(this.articleName);
    this.articleName = null;
  }

}
