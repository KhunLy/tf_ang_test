import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class RealMock {
    getItems(): Observable<string[]>{
        return new Observable<string[]>(o => {
            setTimeout(
                () => o.next(['sel', 'poivre']), 
                2000
            );
        });
    }
}