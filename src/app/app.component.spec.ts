import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {

  let app;
  let fixture;
  let compiled;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
    compiled = fixture.debugElement.nativeElement;
  }));

  afterEach(() => {

  });

  // it('should create the app', () => {
  //   expect(app).toBeTruthy();
  // });

  // it(`should have as title 'demo-test'`, () => {
  //   expect(app.title).toEqual('demo-test');
  // });

  // xit('should render title', () => {
  //   fixture.detectChanges();
  //   expect(compiled.querySelector('.content span').textContent).toContain('demo-test app is running!');
  // });
});
